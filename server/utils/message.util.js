
const moment = require('moment');

var generateMessage = ( from, text, user_id) => {
    return {
        from,
        text,
        user_id,
        created_at: moment().calendar()
    }
};

module.exports = {
    generateMessage
}