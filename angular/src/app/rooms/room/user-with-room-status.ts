export interface UserWithRoomStatus {
    id: string;
    name: string;
    room_allowed_user_id: string;
    room_id: string;
    user_id: String;
}
