const Colors = require('colors');
const Cli = require('commander');
const Exit = require('exit');
const knexConf = require('../knexfile');
const knex = require('knex')(knexConf.development);

const internals = {
    table: null,
    limit: 10
}

Cli.parse(process.argv);

if (!Cli.args[0]) {

    console.log(Colors.red(`\n table argument must be passed, - ex: chat db-show user \n`));
    Exit()
}
internals.table = Cli.args[0];

if (Cli.args[1]) {
    internals.limit = parseInt(Cli.args[1]);
}

knex(internals.table).select('*').limit(internals.limit).then((createdUsers) => {

    console.log(createdUsers)
    Exit()
}).catch((error) => {

    console.log(Colors.red(error));
    Exit()
})