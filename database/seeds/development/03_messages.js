const uuidv4 = require('uuid/v4');

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries

  return knex('message').del().then(function () {

    return knex('user').then((users) => {

      return knex('room').then((rooms) => {

        const messages = [{
            id: uuidv4(),
            text: "Qual o import de Angular Component?",
            user_id: users[0].id,
            room_id: rooms[0].id
          },
          {
            id: uuidv4(),
            text: "import { Component } from '@angular/core';",
            user_id: users[1].id,
            room_id: rooms[0].id
          }
        ];
        // Inserts seed entries
        return knex('message').insert(messages).then(() => {

          return knex('message').then((createdMessage) => {

            //console.log(createdMessage)
          })
        })

      })

    })
  });

};