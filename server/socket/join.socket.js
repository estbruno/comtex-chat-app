module.exports = (io, socket, Model, utils, roomUsers) => {

    socket.on('join', (join_params, callback) => {

        if (!join_params.room_id || !join_params.user_name ||  !join_params.user_id ) {
            callback('Bad request to join');
        }

        /* Verify if room exits*/
        new Model.Room().where('id', join_params.room_id).fetch({ withRelated: ['roomAllowedUser']}).then(function (data) {
            
            const roomData = data.attributes;
            if (!roomData) {
                callback('Room Not Found');
            }

            // checks whether user is allowed to access private room
            if(join_params.user_id != roomData.user_id && !roomData.is_public ){
                var isUserAllowed = roomData.roomAllowedUser.filter((allowedUser) => allowedUser.user_id === join_params.user_id);
                if(isUserAllowed.length === 0){
                    callback('user is not allowed');
                }
            }
            
            socket.join(join_params.room_id);
            roomUsers.removeUser(socket.id);
            roomUsers.addUser(
                socket.id,
                join_params.user_name,
                join_params.room_id,
                join_params.user_id);

            io.to(join_params.room_id).emit('updateUserList', roomUsers.getUserList(join_params.room_id));

             //Send welcome messages to join user
            socket.emit('newMessage', utils.generateMessage('Admin', 'Welcome to the chat app'));

            //Send joined messages to all users
            socket.broadcast.to(join_params.room_id).emit('newMessage', utils.generateMessage('Admin', `${join_params.user_name} has joined.`));
            callback();

        }).catch(function (error) {
            console.log(error);
            callback('An error occured');
        });
    });
}