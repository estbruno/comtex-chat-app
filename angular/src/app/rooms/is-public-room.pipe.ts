import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'isPublicRoom' })
export class IsPublicRoomPipe implements PipeTransform {

    transform(is_public) {
        if (is_public) {
            return 'Public';
        } else {
            return 'Private';
        }
    }
}
