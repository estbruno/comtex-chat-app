const uuidv4 = require('uuid/v4');
const _ = require('lodash');

module.exports = (bookshelf) => {

	var Model = require('./../models')(bookshelf);

    /* Get a room */
	var getMessage= function (req, res) {

		var id = req.params.id;

		new Model.Message().where('id', id)
			.fetch()
			.then(function (data) {

				if (!data) {
					return res.status(404).json('Not found');
				}

				return res.json(data.attributes);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
	};

	/* Get all message */
	var getAllMessages = function (req, res) {
		new Model.Message().query(function (qb) {
				qb.select([
					'*'
				]);
			}).fetchAll()
			.then(function (data) {
				res.json(data);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
	};


	return {
        getMessage,
        getAllMessages
	}
}