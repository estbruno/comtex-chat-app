const uuidv4 = require('uuid/v4');
const utils = require('./../utils');

module.exports = (io, socket, Model) => {

    socket.on('createMessage', (message, callback) => {
   
        new Model.Message().save({
            id: uuidv4(),
            text: message.text,
            user_id: message.user_id,
            room_id: message.room_id
        }).then(function () {
           
            io.emit('newMessage', utils.generateMessage(message.from, message.text, message.user_id));
            callback()
        }).catch(function (error) {
            callback('An error occured')
        });
    })
}