import { Pipe, PipeTransform } from '@angular/core';
import { UserWithRoomStatus } from './room/user-with-room-status';

@Pipe({ name: 'filterRoomUserWithStatusByName' })
export class FilterRoomUserWithStatusByName implements PipeTransform {

    transform(userRoomListWithStatus: UserWithRoomStatus[], nameQuery: string) {
        nameQuery = nameQuery.trim().toLowerCase();
        if (nameQuery) {
            return userRoomListWithStatus.filter(userRoomWithStatus =>
                userRoomWithStatus.name.toLowerCase().includes(nameQuery)
            );
        } else {
            return userRoomListWithStatus;
        }
    }

}
