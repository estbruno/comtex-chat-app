
exports.up = function(knex, Promise) {
  
    return knex.schema.createTable('room_allowed_user', (table) => {

        table.text('id'),
        table.timestamp('created_at').defaultTo(knex.fn.now()),
        table.text('user_id'),
        table.text('room_id'),
        table.primary(['id']);
        table.foreign('user_id').references('user.id');
        table.foreign('room_id').references('room.id');
        table.unique(['user_id', 'room_id'])
    })
};

exports.down = function(knex, Promise) {

    return knex.schema.dropTable('room_allowed_user')
};
