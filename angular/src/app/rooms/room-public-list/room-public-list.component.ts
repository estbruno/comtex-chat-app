import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RoomService } from '../room/room.service';
import { Room } from '../room/room';

@Component({
    templateUrl: './room-public-list.component.html'
})
export class RoomPublicListComponent implements OnInit {

    rooms: Room[];
    constructor(private roomService: RoomService, private router: Router) { }
    ngOnInit(): void {

        this.roomService.listPublicRooms().subscribe(rooms => {

            this.rooms = rooms;
        });
    }
}
