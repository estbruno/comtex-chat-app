import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { RoomService } from '../room/room.service';
import { UserService } from 'src/app/core/user/user.service';
import { UserWithRoomStatus } from '../room/user-with-room-status';

@Component({
    templateUrl: './room-invite-user.component.html'
})
export class RoomInviteUserComponent implements OnInit {

    userId: string;
    userName: string;
    roomId: string;
    userFilter = '';
    roomName: string;
    users: UserWithRoomStatus[];
    constructor(private roomService: RoomService,
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService) { }
    ngOnInit(): void {

        this.roomId = this.route.snapshot.paramMap.get('room_id');
        this.roomName = this.route.snapshot.paramMap.get('room_name');

        this.userId = this.userService.getUserId();
        this.userName = this.userService.getUserName();

        this.roomService.listUserWithInvitationStatusByRoomId(this.roomId).subscribe(userList => {

            this.users = userList;
        });
    }
}
