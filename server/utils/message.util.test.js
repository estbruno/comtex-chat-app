var expect = require('expect');

var {generateMessage} = require('./message.util');

describe('generateMessage', () => {
  it('should generate correct message object', () => {
    var from = 'Jen';
    var text = 'Some message';
    var user_id = '123123';
    var message = generateMessage(from, text, user_id);

    expect(typeof message.created_at).toBe('string');
    expect(message).toMatchObject({from, text, user_id});
  });
});
