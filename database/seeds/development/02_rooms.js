const uuidv4 = require('uuid/v4');

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('room').del().then(function () {

    return knex('user').then((users) => {

      const rooms = [{
          id: uuidv4(),
          name: 'Angular',
          user_id: users[0].id,
          is_public: true
        },
        {
          id: uuidv4(),
          name: 'Node',
          user_id: users[0].id,
          is_public: true
        },
        {
          id: uuidv4(),
          name: 'Engenharia',
          user_id: users[1].id,
          is_public: true
        },
        {
          id: uuidv4(),
          name: 'INB',
          user_id: users[1].id,
          is_public: false
        },
        {
          id: uuidv4(),
          name: 'fisica',
          user_id: users[2].id,
          is_public: true
        },
        {
          id: uuidv4(),
          name: 'Lab_turma_2',
          user_id: users[2].id,
          is_public: false
        }
      ];
      // Inserts seed entries
      return knex('room').insert(rooms).then(() => {

        return knex('room').then((createdRooms) => {

          //console.log(createdRooms)
        })
      })
    })
  });

};