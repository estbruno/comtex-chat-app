'use strict';

const internals = {};

exports.cleanVersion = internals.cleanVersion = (version) => {

    return version.replace(/[=<>v]/g, '');
};

exports.isNotEqualVersion = (currentVersion, requiredVersion) => {

    return internals.cleanVersion(currentVersion) !== internals.cleanVersion(requiredVersion);
};
