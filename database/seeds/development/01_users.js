const uuidv4 = require('uuid/v4');
const Password = require('../../../lib/password-utils');

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('user').del().then(function () {

    const users = [{
        id: uuidv4(),
        name: 'Estevão Bruno',
        email: 'estbruno@gmail.com',
        password: Password.cryptSync('1234')
      },
      {
        id: uuidv4(),
        name: 'Daiana Lima',
        email: 'daiana@gmail.com',
        password: Password.cryptSync('teste12')
      },
      {
        id: uuidv4(),
        name: 'Ana luiza',
        email: 'analuiza@gmail.com',
        password: Password.cryptSync('teste1')
      }
    ];
    // Inserts seed entries
    return knex('user').insert(users).then(() => {

      return knex('user').then((createdUsers) => {

        //console.log(createdUsers)
      })
    })
  });
};