export interface RoomFull {
    id: string;
    name: string;
    user_id?: string;
    is_public: boolean;
    user?: {
        id: string,
        name: string
    };
}
