const uuidv4 = require('uuid/v4');
const _ = require('lodash');

module.exports = (bookshelf) => {

	var Model = require('./../models')(bookshelf);

	/* Save a room */
	var saveRoom = function (req, res) {

		if (!req.body.name || !req.body.user_id || req.body.is_public === undefined) {
			return res.status(400).json('Bad Request');
		}

		var newData = {
			id: uuidv4(),
			name: req.body.name,
			is_public: req.body.is_public,
			user_id: req.body.user_id
		}

		/* verify if user exists*/
		new Model.User().where('id', newData.user_id).fetch().then((userData) => {

			if (!userData) {
				return res.status(404).json('User Not found');
			}

			new Model.Room().save(newData).then((data) => {
				res.json(data);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});

		}).catch(function (error) {
			console.log(error);
			res.status(500).json('An error occured')
		});

	};

	/* Get a room */
	var getRoom = function (req, res) {
		var roomId = req.params.id;

		new Model.Room().where('id', roomId)
			.fetch({
				withRelated: ['user']
			})
			.then(function (data) {
				if (!data) {
					return res.status(404).json('Not found');
				}

				var dataToReturn = _.pick(data.attributes, ['id', 'name', 'is_public', 'user']);

				return res.json(dataToReturn);

			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
	};

	/* Get all room */
	var getAllRoom = function (req, res) {

		new Model.Room().query(function (qb) {
				qb.select([
					'id', 'name', 'is_public'
				]);
			}).fetchAll()
			.then(function (data) {

				res.json(data);

			}).catch(function (error) {

				console.log(error);
				res.status(500).json('An error occured');

			});
	};

	/* Get all public room */
	var getAllPublicRoom = function (req, res) {

		new Model.Room().query(function (qb) {

				qb.select([
					'room.id as room_id', 'room.name as room_name', 'user.name as user_name','is_public'
				]);
				qb.leftJoin('user', 'room.user_id', 'user.id');
				qb.where('is_public', '=', true);
			}).orderBy('name').fetchPage({
				pageSize: 15,
				page: 1
			})
			.then(function (data) {

				res.json(data);
			}).catch(function (error) {

				console.log(error);
				res.status(500).json('An error occured');
			});
	};

	/* Get  room by user*/
	var getRoomsByUser = function (req, res) {

		/* verify if user exists*/
		new Model.User().where('id', req.params.user_id).fetch().then((userData) => {

			if (!userData) {
				return res.status(404).json('User Not found');
			}

			new Model.Room().query(function (qb) {

					qb.select([
						'room.id as room_id', 'room.name as room_name', 'user.name as user_name','is_public'
					]);
					qb.leftJoin('user', 'room.user_id', 'user.id');
					qb.where('user_id', '=', req.params.user_id);
				}).orderBy('name').fetchPage({
					pageSize: 15,
					page: 1
				})
				.then(function (data) {

					res.json(data);
				}).catch(function (error) {

					console.log(error);
					res.status(500).json('An error occured');
				});

		}).catch(function (error) {
			console.log(error);
			res.status(500).json('An error occured')
		});
	};

	/* Get rooms that the user is invited to*/
	var getGuestRoomsByUser = function (req, res) {

		/* verify if user exists*/
		new Model.User().where('id', req.params.user_id).fetch().then((userData) => {

			if (!userData) {
				return res.status(404).json('User Not found');
			}

			new Model.Room().query(function (qb) {

					qb.select([
						'room.id as room_id', 
						'room.name as room_name',
						'user.name as user_name',
						'is_public'
					]);
					qb.leftJoin('room_allowed_user', 'room.id', 'room_allowed_user.room_id');
					qb.leftJoin('user', 'room.user_id', 'user.id');
					qb.where('room_allowed_user.user_id', '=', req.params.user_id);
				}).orderBy('name').fetchPage({
					pageSize: 15,
					page: 1
				})
				.then(function (data) {

					res.json(data);
				}).catch(function (error) {

					console.log(error);
					res.status(500).json('An error occured');
				});

		}).catch(function (error) {
			console.log(error);
			res.status(500).json('An error occured')
		});
	};

	/* Delete a room */
	var deleteRoom = function (req, res) {

		var id = req.params.id;

		new Model.Room().where('id', id)
			.destroy()
			.then(function () {
				res.json('room deleted');
			})
			.catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
	};

	/* Update a room */
	var updateRoom = function (req, res) {

		if (!req.params.id || !req.body.name || req.body.is_public === undefined) {
			return res.status(400).json('Bad Request');
		}

		new Model.Room().where('id', req.params.id).fetch().then(function (data) {
			if (!data) {
				return res.status(404).json('Not found');
			}

			new Model.Room({
				id: req.params.id
			}).save({
				name: req.body.name,
				is_public: req.body.is_public
			}, {
				patch: true
			}).then(() => {

				res.json('room updated');
			}).catch((err) => {

				console.log(error);
				res.status(500).json('An error occured');
			});

		}).catch(function (error) {
			console.log(error);
			res.status(500).json('An error occured');
		});
	};

	/* Invite user to a room */
	var addUsertoRoom = function (req, res) {

		if (!req.body.user_id || !req.body.room_id) {
			return res.status(400).json('Bad Request');
		}

		var userId = req.body.user_id;
		var roomId = req.body.room_id

		new Model.RoomAllowedUser().where({user_id: userId, room_id: roomId}).fetch().then(function (data) {
			if (data) {
				return res.json(data.id);
			}

			new Model.RoomAllowedUser().save({id: uuidv4(), user_id: userId, room_id: roomId}).then((savedData) => {
				res.json(savedData.id);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json("An error occured");
			});

		}).catch(function (error) {
			console.log(error);
			res.status(500).json('An error occured');
		});
	};

	/* remove Invite user to a room */
	var removeUsertoRoom = function (req, res) {

		if (!req.params.user_id || !req.params.room_id) {
			return res.status(400).json("Bad Request");
		}

		var userId = req.params.user_id;
		var roomId = req.params.room_id

		new Model.RoomAllowedUser().where({user_id: userId, room_id: roomId}).fetch().then(function (data) {
			if (!data) {
				return res.json("user was not invited");
			}

			new Model.RoomAllowedUser().where('id', data.id)
			.destroy()
			.then(function () {
				res.json("invitation removed");
			})
			.catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});

		}).catch(function (error) {
			console.log(error);
			res.status(500).json('An error occured');
		});
	};


	return {
		saveRoom,
		getAllRoom,
		getRoom,
		getAllPublicRoom,
		getRoomsByUser,
		deleteRoom,
		updateRoom,
		getGuestRoomsByUser,
		addUsertoRoom,
		removeUsertoRoom
	}
}