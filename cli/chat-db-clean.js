
const Fs = require('fs-extra');
const Path = require('path');
const Colors = require('colors');
const Exit = require('exit');

const DB_DATA_PATH =  Path.join(__dirname ,'..','database','data');

Fs.emptyDir(DB_DATA_PATH).then(() => {

    console.log(Colors.green(`\n Database file successfully removed \n`));
    Exit()
}).catch((err) => {

    console.log(Colors.red(`\n ${err} \n`));
    Exit()
});