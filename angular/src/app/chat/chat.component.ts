import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import io from 'socket.io-client';

import { UserService } from '../core/user/user.service';
import { User } from '../core/user/user';
import { ChatMessage } from './message/chat-message';
import { environment } from './../../environments/environment';
import { ChatUser } from './users/chat-user';

@Component({
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {

    chatMessageForm: FormGroup;
    roomId;
    roomName;
    socket;
    @ViewChild('chatMessageInput') chatMessageInput: ElementRef<HTMLInputElement>;
    user$: Observable<User>;
    chatMessages: ChatMessage[] = [];
    chatUsers: ChatUser[] = [];
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private userService: UserService) {
        this.socket = io(environment.socket_url);
        this.roomId = route.snapshot.paramMap.get('room_id');
        this.roomName = route.snapshot.paramMap.get('room_name');
        this.user$ = userService.getUser();
    }
    ngOnInit(): void {
        this.chatMessageForm = this.formBuilder.group({
            message: ['', Validators.required]
        });

        this.socket.on('connect', () => {
            console.log('connected to server to chat');

            const join_params = {
                user_id: this.userService.getUserId(),
                user_name: this.userService.getUserName(),
                room_id: this.roomId
            };

            const angularThis = this;
            this.socket.emit('join', join_params, function (err) {
                if (err) {
                    alert(err);
                    angularThis.router.navigate(['/rooms']);
                }
            });
        });

        this.socket.on('updateUserList', (users) => {
            this.chatUsers = users;
        });

        this.socket.on('disconnect', () => {
            console.log('disconnect to chat server');
        });
        this.socket.on('newMessage', (message) => {
            this.chatMessages.push(message);
            this.scrollToBottonContainerMessages();
        });
    }

    ngOnDestroy(): void {

        this.socket.close();
    }

    sendMessage() {

        const messageToSend: ChatMessage = {
            from: this.userService.getUserName(),
            user_id: this.userService.getUserId(),
            text: this.chatMessageForm.get('message').value,
            room_id: this.roomId,
            created_at: new Date()
        };

        this.socket.emit('createMessage', messageToSend, () => {
            this.chatMessageForm.reset();
        });
    }

    scrollToBottonContainerMessages() {

        setTimeout(() => {
            try {
                const chatMessageContainer = document.getElementById('messages');
                chatMessageContainer.scrollTop = chatMessageContainer.scrollHeight - chatMessageContainer.clientHeight;
            } catch (error) { }
        }, 300);
    }
}
