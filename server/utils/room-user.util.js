

class RoomUsers {
  constructor () {
    this.users = [];
  }
  addUser (id, name, room_id,user_id) {
    var user = {id, name, room_id,user_id};
    this.users.push(user);
    return user;
  }
  removeUser (id) {
    var user = this.getUser(id);

    if (user) {
      this.users = this.users.filter((user) => user.id !== id);
    }

    return user;
  }
  getUser (id) {
    return this.users.filter((user) => user.id === id)[0]
  }
  getUserList (room_id) {
    var users = this.users.filter((user) => user.room_id === room_id);

    return users;
  }
}

module.exports = {RoomUsers};
