import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Room } from '../room/room';

@Component({
    selector: 'app-room-list',
    templateUrl: './room-list.component.html'
})
export class RoomListComponent {

    @Input() rooms: Room[];
    @Input() showMenu = false;
    constructor( private router: Router) { }
}
