module.exports = (bookshelf) => {

	const userModel = require('./user.model')(bookshelf);
	const RoomAllowedUserModel = require('./room_allowed_user.model')(bookshelf);

	return bookshelf.Model.extend({
		tableName: 'room',

		initialize: function () {

			this.on('fetched', this.loadUser);
			this.on('fetched', this.loadRoomAllowedUser);
		},

		loadUser: function () {

			if (!this.relations.user) {
				return;
			}
			this.attributes.user = {
				id: this.relations.user.attributes.id,
				name: this.relations.user.attributes.name
			};
		},

		loadRoomAllowedUser: function () {

            if (!this.relations.roomAllowedUser) {
                return;
            }

            const RoomAllowedUsers = [];

            this.relations.roomAllowedUser.forEach((roomAllowedUser) => {

                RoomAllowedUsers.push(roomAllowedUser.attributes);
            });

            this.attributes.roomAllowedUser = RoomAllowedUsers;
        },

		user: function () {
			return this.belongsTo(userModel);
		},

		roomAllowedUser: function () {
			return this.hasMany(RoomAllowedUserModel);
		}
	})
}