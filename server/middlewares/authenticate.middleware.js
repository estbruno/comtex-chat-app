const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  const token = req.headers['x-access-token'];
  console.log(token)
  if (token) {

    try {
      var decoded = jwt.verify(token, req.app.get('jwt_secret'));
      console.log(`Valid token received: ${token}`);
      req.user = decoded;
      next();
    } catch (err) {
      console.log(err);
      console.log(`Invalid token: ${token}`);
      return res.sendStatus(401);
    }
  } else {
    console.log('Toke is missing!');
    return res.sendStatus(401);
  }
};