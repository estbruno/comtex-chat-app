import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RoomPublicListComponent } from './room-public-list/room-public-list.component';
import { RoomListComponent } from './room-list/room-list.component';
import { RoomUserListComponent } from './room-user-list/room-user-list.component';
import { RoomService } from './room/room.service';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';
import { RoomFormComponent } from './room-form/room-form.component';
import { RoomDeleteComponent } from './room-delete/room-delete.component';
import { IsPublicRoomPipe } from './is-public-room.pipe';
import { RoomGuestListComponent } from './room-guest-list/room-guest-list.component';
import { RoomInviteUserComponent } from './room-invite-user/room-invite-user.component';
import { FilterRoomUserWithStatusByName } from './filter-room-user-with-status-by-name.pipe';
import { UserListWithStatusComponent } from './room-invite-user/user-with-status-list/user-with-status-list.component';

@NgModule({
    declarations: [
        RoomPublicListComponent,
        RoomListComponent,
        RoomUserListComponent,
        RoomFormComponent,
        RoomDeleteComponent,
        RoomGuestListComponent,
        RoomInviteUserComponent,
        UserListWithStatusComponent,
        IsPublicRoomPipe,
        FilterRoomUserWithStatusByName
    ],
    exports: [
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        VMessageModule
    ],
    providers: [
        RoomService
    ]
})
export class RoomsModule { }
