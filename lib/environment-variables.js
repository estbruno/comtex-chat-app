'use strict';

const Joi = require('joi');

const internals = {};

internals.COMMON_KEYS = {
    NODE_ENV: Joi.string().required().valid('development', 'production','test'),
    WEB_HOST: Joi.string().ip().required(),
    WEB_PORT: Joi.number().required(),
    API_HOST: Joi.string().ip().required(),
    API_PORT: Joi.number().required(),
    DATABASE_PORT: Joi.number().required(),
    DATABASE_HOST: Joi.string().ip().required(),
    DATABASE_USER: Joi.string().required(),
    DATABASE_PASS: Joi.string().required(),
    DATABASE_NAME: Joi.string().required(),
    DATABASE_APPLICATION_NAME: Joi.string().required(),
    AUTH_PASSWORD: Joi.string().min(32).required(),
};


// Adicionar variáveis exclusivas do modo de execução desenvolvimento
internals.DEVELOPMENT_KEYS = {
};


exports.isValidProductionEnv = (env) => {

    return internals.isValid(env, internals.COMMON_KEYS);
};


exports.isValidDevelopmentEnv = internals.isValidDevelopmentEnv = (env) => {

    const KEYS = Object.assign(internals.COMMON_KEYS, internals.DEVELOPMENT_KEYS);
    return internals.isValid(env, KEYS);
};


exports.isNotValidDevelopmentEnv = (env) => {

    return !internals.isValidDevelopmentEnv(env);
};


internals.isValid = (env, requiredKeys) => {

    const schema = Joi.object().keys(requiredKeys);
    const variables = {};

    Object.keys(requiredKeys).forEach((key) => {

        if (env.hasOwnProperty(key)) {
            variables[key] = env[key];
        }
    });

    return Joi.validate(variables, schema).error === null;
};
