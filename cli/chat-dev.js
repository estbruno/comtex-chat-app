'use strict';

const Exit = require('exit');
const Colors = require('colors');
const Nodemon = require('nodemon');
const Path = require('path');

Nodemon({
    script: Path.join(__dirname, '../server/index.js')
});

Nodemon.on('start', () => {

    console.log(Colors.green('\nInitialized application...'));

}).on('quit', () => {

    console.log(Colors.green('\nApplication Terminated...'));
    Exit();
}).on('restart', (files) => {

    console.log(Colors.green('\nApplication restarted...'));
});