
const DBConfig = require('../knexfile');

require('dotenv').config();

var knex = require('knex')(DBConfig[process.env.NODE_ENV]);
var bookshelf = require('bookshelf')(knex);

bookshelf.plugin('pagination');

module.exports.bookshelf = bookshelf;
