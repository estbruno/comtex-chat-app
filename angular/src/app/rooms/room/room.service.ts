import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Room } from './room';
import { environment } from './../../../environments/environment';
import { NewRoom } from './new-room';
import { RoomFull } from './room-full';
import { UserWithRoomStatus } from './user-with-room-status';


const API_URL = environment.api_url;

@Injectable()
export class RoomService {

    constructor(private http: HttpClient) { }

    getRoom(roomId: string) {
        return this.http
            .get<RoomFull>(`${API_URL}/room/${roomId}`);
    }

    listPublicRooms() {
        return this.http
            .get<Room[]>(API_URL + '/room/all/public');
    }

    listRoomsByUserId(userId: string) {
        return this.http
            .get<Room[]>(`${API_URL}/room/user/${userId}`);
    }

    listGuestRoomsByUserId(userId: string) {
        return this.http
            .get<Room[]>(`${API_URL}/room/guest/${userId}`);
    }

    newRoom(newRoom: NewRoom) {
        return this.http.post(API_URL + '/room', newRoom);
    }

    deleteRoom(roomId: string) {
        return this.http.delete<string>(`${API_URL}/room/${roomId}`);
    }

    updateRoom(room: RoomFull) {
        return this.http.put<string>(`${API_URL}/room/${room.id}`, room);
    }

    listUserWithInvitationStatusByRoomId(roomId: string) {
        return this.http.get<UserWithRoomStatus[]>(`${API_URL}/user/withinvitestatus/${roomId}`);
    }

    addUsertoRoom(roomId: string, userId: string) {
        // /room/:room_id/addinvite/user/:user_id
        return this.http.post<string>(`${API_URL}/room/addinvite`, { room_id: roomId, user_id: userId });
    }

    removeUsertoRoom(roomId: string, userId: string) {
        // /room/:room_id/removeinvite/user/:user_id
        return this.http.delete<string>(`${API_URL}/room/${roomId}/removeinvite/user/${userId}`);
    }
}
