const uuidv4 = require('uuid/v4');

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries

  return knex('room_allowed_user').del().then(function () {

    return knex('user').then((users) => {

      return knex('room').then((rooms) => {

        const room_allowed_user_list = [{
            id: uuidv4(),
            user_id: users[0].id,
            room_id: rooms[3].id
          },
          {
            id: uuidv4(),
            user_id: users[0].id,
            room_id: rooms[5].id
          },
          {
            id: uuidv4(),
            user_id: users[1].id,
            room_id: rooms[5].id
          }
        ];
        // Inserts seed entries
        return knex('room_allowed_user').insert(room_allowed_user_list).then(() => {

          return knex('room_allowed_user').then((created_room_allowed_user_list) => {

            // console.log(created_room_allowed_user_list)
          })
        })

      })

    })
  });

};