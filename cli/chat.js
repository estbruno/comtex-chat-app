#!/usr/bin/env node
'use strict';

const Cli = require('commander');

Cli
    .version(require('../package.json').version)
    .command('dev', 'initializes application in development').alias('d')
    .command('db-seed', 'loads seed for database').alias('seed')
    .command('db-clean', 'Remove file from sqlite database')
    .command('db-create', 'Cria o banco de dados com migrations e seeds')
    .command('db-show <table> <maxrows>', 'View data from the database past the table and the maximum number of rows')
    .parse(process.argv);


