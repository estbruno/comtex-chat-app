export interface ChatMessage {
    room_id: string;
    user_id: string;
    from: string;
    text: string;
    created_at: Date;
}
