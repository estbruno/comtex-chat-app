import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RoomService } from '../room/room.service';
import { Room } from '../room/room';
import { UserService } from 'src/app/core/user/user.service';

@Component({
    templateUrl: './room-guest-list.component.html'
})
export class RoomGuestListComponent implements OnInit {

    // Data
    rooms: Room[] = [];
    userId: string;
    userName: string;

    constructor(
        private roomService: RoomService,
        private router: Router,
        private userService: UserService) { }
    ngOnInit(): void {

        this.userId = this.userService.getUserId();
        this.userName = this.userService.getUserName();
        this.roomService.listGuestRoomsByUserId(this.userId).subscribe(rooms => {

            this.rooms = rooms;
        });
    }
}
