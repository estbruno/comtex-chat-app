import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { RoomService } from '../room/room.service';
import { UserService } from 'src/app/core/user/user.service';
import { RoomFull } from '../room/room-full';

@Component({
    templateUrl: './room-delete.component.html'
})
export class RoomDeleteComponent implements OnInit {

    userId: string;
    userName: string;
    roomId: string;
    room: RoomFull;
    constructor(private roomService: RoomService,
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService) { }
    ngOnInit(): void {

        this.roomId = this.route.snapshot.paramMap.get('room_id');
        this.userId = this.userService.getUserId();
        this.userName = this.userService.getUserName();
        this.roomService.getRoom(this.roomId).subscribe(

            room => this.room = room,
            err => {
                this.router.navigate(['']);
            }
        );
    }

    deleteRoom() {

        this.roomService.deleteRoom(this.roomId).subscribe(
            () => this.router.navigate(['/rooms', 'user']),
            err => {
                console.log(err);
                alert('Error');
            }
        );
    }
}
