# Chat-app-comtex 

Chat app em Nodejs e Angular 6
- - -

## Índice

* Requisitos
* Instalação e execução demo mod
* Instalação dev mod
* Programa chat
* Configurações
* Destaques do programa chat de linha de comando
* Pontos extras

- - -

## Requisitos

Requisitos para o ambiente de desenvolvimento.

* Node.js 8.9.4
* Git

- - -

## Instalação e execução demo mod

Repositório já possui os arquivos do frontend angular e o database.

**1)** Clonar repositório:

```sh
$ git clone https://estbruno@bitbucket.org/brunobgw/chat_estevao.git
```

**2)** Instalar dependências:

```sh
$ cd chat-app-comtex
$ npm install
```

**3)** Executar via npm
```sh
$ npm start
```

- - -

## Instalação dev mod

**1)** Clonar repositório:

```sh
$ git clone https://estbruno@bitbucket.org/brunobgw/chat_estevao.git
```

**2)** Instalar dependências:

```sh
$ cd chat-app-comtex
$ npm install
```

**3)** Instalar [knex.js](https://knexjs.org/) global (para manipulação do banco de dados):

```sh
$ npm install -g knex
```

**4)** Colocar programa 'chat' no PATH do sistema:

```sh
$ npm link
```

**5)** Habilitar variáveis de ambiente:

Editar .env e angular/environment, nestes arquivo estão declaradas as variáveis de ambiente usadas pela aplicação no ambiente de desenvolvimento.

**6)** Instação dependências Angular

```sh
$ cd angular
$ npm install
```

**7)** Instação Angular cli

```sh
$ npm install -g @angular/cli@6.0.7
```
- - -

## Programa chat

O programa chat é um utilitário de de linha de comando que disponibiliza diversos recursos para auxiliar nas tarefas de desenvolvimento, para ver os comandos disponíveis usa o help:

```sh
$ chat help
```

- - -

## Arquitetura do sistema

O projeto é essencialmente uma aplicação [express.js](https://expressjs.com/) e [angular 6](https://v6.angular.io/docs)

* **angular** - Sub-projeto do frontend em Angular
    * **src** - código fonte aplicação angular
        * **app** - Arquivos da aplicação
        * **assets** - Arquivos estáticos
        * **environments** - arquivos de ambiente (obs: em ambiente de desenvolviment environments.api_url e socket_url deve ser o mesmo do backend) 
* **cli** - Arquivos do cli (linhas de comando)
* **database** - Arquivos relacionados ao banco de dados (foi utilizado sqlite para simplificar a instalação)
    * **data** - arquivo do banco de dados embarcado fica nesta pasta.
    * **migrations** - arquivos de migração do banco
    * **relational-model** - Modelo relacional do banco de dados (ferramenta utilizada: [pgModeler](https://pgmodeler.io/) )
    * **seeds** - arquivos seeds separados por ambiente
* **lib** - Modulos gerais da aplicação
    * **database.js** - arquivo inicializador da instância lib [bookshelf.js](https://bookshelfjs.org/)
    * **environment-variables.js** - validados das variáveis do .env file
    * **node-version.js** - verificador da versão do nodejs
    * **password-util.js** - lib que manipula senha dos usuários
* **server** - Arquivos do servidor backend
    * **middlewares** - módulos dos 'plugins' para express que tem acesso ao request e response
    * **models** - mapeamento do banco usando lib [bookshelf.js](https://bookshelfjs.org/)
    * **public** - Arquivos estáticos da aplicação (aqui fica os arquivos build do app angular)
    * **routes** - mapeamento das rotas (api, aplicação angular, etc), ferramenta utilizada para testar api: [POSTMAN](https://www.getpostman.com/)
    * **socket** - arquivos do modulo para usar websocket
    * **util** - arquivos de auxílio
    * **index.js** - arquivo de inicialização do express
    * **router.js** - arquivo que juntas todas as rotas para o express
* **.env** - arquivo que contém variáveis de ambiente da aplicação
* **knexfile.js** - arquivo de configuração [knex](https://knexjs.org/) de conexão do banco de dados. 

- - -

## Destaques do programa chat de linha de comando

Conforme mencionado basta executar ```chat help``` no terminal para ver a lista completa de comandos disponíveis.

Comandos para interagir o serviço de banco de dados:

* **db-clean** - Limpa banco de dados
* **db-create** - Cria banco de dados (gera arquivo, roda migrações e roda seeds )
* **db-seed** - Executa os dados iniciais da aplicação
* **db-show** - Mostra dos dados passando tabela como parametro

Comandos para interagir com a aplicação:

* **dev** - inicializa uma seção de desenvolvimento

## Pontos Extras

* **Histórico do chat em banco de dados de sua preferência** - Mensagens são salvas no socket "createMessage" no seguinte caminho "./server/socket/create-message.socket.js" 
* **Chamar um usuário para sala privada (onde apenas os usuários convidados podem entrar)** - Usuario possui suas salas e quando a mesma é privada o usuario pode convidar ou remover os convites. O usuario que é convidado vê seus convites da 'Guest Room"
* **Modelo de Entidade Relacionamento** - Esta na pasta "database/relational-model/v0.2/"
* **Integração via REST** - as rotas ficam no arquivo "server/router.js"
