// libs
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const bookshelf = require('./../lib/database').bookshelf;
const cors = require('cors');
const middlewares = require('./middlewares');

const corsOptions = {
  exposedHeaders: ['x-access-token']
};
var app = express();

// Configs 
require('dotenv').config();

app.use(bodyParser.json());
app.use(cors(corsOptions));
app.set('jwt_secret', process.env.JWT_SECRET);

//routes
app.use('/', express.static(__dirname + '/public/angular'));
require('./router')(app,bookshelf,middlewares);


var server = http.createServer(app);

//Websocket
require('./socket')(server,bookshelf);

const expressPort = process.env.SERVER_PORT || 3000;
server.listen(expressPort, () => {
  console.log(`Started up at port ${expressPort}`);
});