var path = require('path');

var angularApp = function (req, res) {
	res.sendFile(path.resolve(__dirname + '/../public//angular/index.html'));
};

module.exports = (bookshelf) =>{
	return {
		index: angularApp
	}
}