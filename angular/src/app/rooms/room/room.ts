export interface Room {
    room_id: string;
    room_name: string;
    user_name: string;
    is_public: boolean;
}
