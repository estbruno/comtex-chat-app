export interface NewRoom {
    name: string;
    is_public: string;
    user_id: string;
}
