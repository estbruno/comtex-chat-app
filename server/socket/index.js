const socketIO = require('socket.io');
const utils = require('./../utils');

module.exports = function (server,bookshelf) {
	var io = socketIO(server);
	var Model = require('./../models')(bookshelf);
	var roomUsers = new utils.roomUsers();

	io.on('connection', (socket) => {

		require('./join.socket')(io,socket,Model,utils,roomUsers)

		require('./create-message.socket')(io,socket,Model,utils)

		socket.on('disconnect', () => {
			var user = roomUsers.removeUser(socket.id);
			if(user){
				io.to(user.room_id).emit('updateUserList',roomUsers.getUserList(user.room_id));
				io.to(user.room_id).emit(
					'newMessage',
					utils.generateMessage('Admin',`${user.name} has left.`,user.user_id)
					);
			}
		})
	});
};