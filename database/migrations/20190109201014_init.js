exports.up = function (knex, Promise) {

    return knex.schema.raw('PRAGMA foreign_keys=ON').then(() => {

        return knex.schema.createTable('user', (table) => {

            table.text('id'),
            table.string('name', 100),
            table.text('email'),
            table.text('password'),
            table.timestamp('created_at').defaultTo(knex.fn.now()),
            table.primary(['id']);
        }).then(() => {

            return knex.schema.createTable('room', (table) => {

                table.text('id'),
                table.string('name', 100),
                table.timestamp('created_at').defaultTo(knex.fn.now()),
                table.boolean('is_public'),
                table.text('user_id'),
                table.primary(['id']);
                table.foreign('user_id').references('user.id');
            }).then(() => {

                return knex.schema.createTable('message', (table) => {

                    table.text('id'),
                    table.text('text'),
                    table.timestamp('created_at').defaultTo(knex.fn.now()),
                    table.text('user_id'),
                    table.text('room_id'),
                    table.primary(['id']);
                    table.foreign('user_id').references('user.id');
                    table.foreign('room_id').references('room.id');
                })
            });
        });

    });

};

exports.down = function (knex, Promise) {

    return knex.schema.dropTable('message').then(() => {

        return knex.schema.dropTable('room').then(() => {

            return knex.schema.dropTable('user')
        });
    });
};