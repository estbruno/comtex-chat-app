import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserNotTakenValidatorService } from './user-not-taken.validator.service';
import { NewUser } from './new-user';
import { SignUpService } from './signup.service';
import { Router } from '@angular/router';
import { PlatformDetectorService } from 'src/app/core/plataform-detector/platform-detector.service';
import { AuthService } from 'src/app/core/auth/auth.service';

@Component({
    templateUrl: './signup.component.html',
    providers: [UserNotTakenValidatorService]
})
export class SignUpComponent implements OnInit {

    signupForm: FormGroup;
    @ViewChild('inputEmail') userEmailInput: ElementRef<HTMLInputElement>;

    constructor(
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private userNotTakenValidatorService: UserNotTakenValidatorService,
        private signUpService: SignUpService,
        private router: Router,
        private platformDetectorService: PlatformDetectorService) { }

    ngOnInit(): void {
        this.signupForm = this.formBuilder.group({
            email: ['',
                [
                    Validators.required,
                    Validators.email
                ],
                this.userNotTakenValidatorService.checkUserEmailTaken()
            ],
            name: ['',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(30)
                ]
            ],
            password: ['',
                [
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(14)
                ]
            ]
        });
        if (this.platformDetectorService.isPlatformBrowser()) {
            this.userEmailInput.nativeElement.focus();
        }
    }

    signup() {
        const newUser = this.signupForm.getRawValue() as NewUser;
        this.signUpService
            .signup(newUser)
            .subscribe(
                () => this.singin(newUser),
                err => console.log(err)
            );
    }

    singin(user: NewUser) {

        this.authService
            .authenticate(user.email, user.password)
            .subscribe(
                () => this.router.navigate(['rooms']),
                err => {
                    alert('Invalid user name or password');
                }
            );
    }
}
