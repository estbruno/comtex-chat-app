'use strict';

const Bcrypt = require('bcrypt');

const internals = {
    saltRounds: 10
};


// Retorna senha criptografada (assíncrono)
exports.crypt = (password) => {

    return new Promise((resolve, reject) => {

        Bcrypt.hash(password, internals.saltRounds).then((hash) => {

            return resolve(hash);
        }).catch((err) => {

            return reject(err);
        });
    });
};


// Retorna senha encriptada (síncrono)
exports.cryptSync = (password) => {

    const salt = Bcrypt.genSaltSync(internals.saltRounds);
    return Bcrypt.hashSync(password, salt);
};


// Compara senha sem criptografia com senha encriptada (assíncrono)
exports.compare = (password, hash) => {

    return new Promise((resolve, reject) => {

        Bcrypt.compare(password, hash).then((res) => {

            return resolve(res);
        }).catch((err) => {

            return reject(err);
        });
    });
};


// Compara senha sem criptografia com senha encriptada (síncrono)
exports.compareSync = (password, hash) => {

    return Bcrypt.compareSync(password, hash);
};
