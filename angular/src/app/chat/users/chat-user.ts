export interface ChatUser {
    id: string;
    room_id: string;
    name: string;
    user_id: string;
}
