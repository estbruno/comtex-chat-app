
module.exports = (bookshelf) =>{

	return {
                User: require('./user.model')(bookshelf),
                Room: require('./room.model')(bookshelf),
                Message: require('./message.model')(bookshelf),
                RoomAllowedUser: require('./room_allowed_user.model')(bookshelf)
	}
}
