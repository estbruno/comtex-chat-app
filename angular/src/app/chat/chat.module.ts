
import { NgModule } from '@angular/core';
import { ChatComponent } from './chat.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ChatMessageComponent } from './message/chat-message.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        ChatComponent,
        ChatMessageComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    exports: [
        ChatComponent
    ]
})
export class ChatModule { }
