const expect = require('expect');

const {
  RoomUsers
} = require('./room-user.util');

describe('Users', () => {
  var users;

  beforeEach(() => {
    users = new RoomUsers();
    users.users = [{
      id: '1',
      name: 'Mike',
      room_id: '1'
    }, {
      id: '2',
      name: 'Jen',
      room_id: '2'
    }, {
      id: '3',
      name: 'Julie',
      room_id: '1'
    }];
  });

  it('should add new user', () => {
    var users = new RoomUsers();
    var user = {
      id: '4',
      name: 'Andrew',
      room_id: '4'
    };
    var resUser = users.addUser(user.id, user.name, user.room_id);

    expect(users.users).toEqual([user]);
  });

  it('should remove a user', () => {
    var userId = '1';
    var user = users.removeUser(userId);

    expect(user.id).toBe(userId);
    expect(users.users.length).toBe(2);
  });

  it('should not remove user', () => {
    var userId = '99';
    var user = users.removeUser(userId);

    expect(user).toBeFalsy();
    expect(users.users.length).toBe(3);
  });

  it('should find user', () => {
    var userId = '2';
    var user = users.getUser(userId);

    expect(user.id).toBe(userId);
  });

  it('should not find user', () => {
    var userId = '99';
    var user = users.getUser(userId);

    expect(user).toBeFalsy();
  });

  it('should return names for room_id 1', () => {
    var userList = users.getUserList('1');

    expect(userList).toEqual([{
      id: '1',
      name: 'Mike',
      room_id: '1'
    }, {
      id: '3',
      name: 'Julie',
      room_id: '1'
    }]);
  });

  it('should return names for room_id 2', () => {
    var userList = users.getUserList('2');

    expect(userList).toEqual([{
      id: '2',
      name: 'Jen',
      room_id: '2'
    }]);
  });
});