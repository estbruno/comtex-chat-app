import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { SignInComponent } from './signin/signin.component';
import { SignUpComponent } from './singup/singup.component';
import { AuthRedirect } from '../core/auth/auth.redirect';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthRedirect],
        children: [
            {
                path: '',
                component: SignInComponent,
                canActivate: [AuthRedirect]
            },
            {
                path: 'signup',
                component: SignUpComponent,
                canActivate: [AuthRedirect]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class HomeRoutingModule { }

