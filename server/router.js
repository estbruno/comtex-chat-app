

module.exports = function (app,bookshelf,middlewares) {

	var user = require('./routes/user.route') (bookshelf);
	var room = require('./routes/room.route')(bookshelf);
	var message = require('./routes/message.route')(bookshelf);
	var angularApp = require('./routes/angular-app.route') (bookshelf);

	const API_PREFIX = '/service';

	/* Index(main) route */
	app.get('/', angularApp.index);

	/* User Routes */
	app.post(API_PREFIX +'/user', user.saveUser);
	app.post(API_PREFIX +'/user/login', user.loginUser);
	app.delete(API_PREFIX +'/user/:id', user.deleteUser);
	app.get(API_PREFIX +'/user', user.getAllUsers);
	app.get(API_PREFIX +'/user/withinvitestatus/:room_id', user.getAllUsersWithInviteStatusByRoomId);		
	app.get(API_PREFIX +'/user/:id', user.getUser);
	app.get(API_PREFIX +'/user/exists/:email', user.isUserExist);

	/* Room Routes */
	app.post(API_PREFIX +'/room',room.saveRoom);
	app.post(API_PREFIX +'/room/addinvite',room.addUsertoRoom);
	app.get(API_PREFIX +'/room', room.getAllRoom);
	app.get(API_PREFIX +'/room/:id', room.getRoom);
	app.get(API_PREFIX +'/room/all/public', room.getAllPublicRoom);
	app.get(API_PREFIX +'/room/user/:user_id', room.getRoomsByUser);
	app.get(API_PREFIX +'/room/guest/:user_id', room.getGuestRoomsByUser);
	app.delete(API_PREFIX +'/room/:id',room.deleteRoom);
	app.delete(API_PREFIX +'/room/:room_id/removeinvite/user/:user_id',room.removeUsertoRoom);
	app.put(API_PREFIX +'/room/:id',room.updateRoom);
	
	/* Message Routes */
	app.get(API_PREFIX +'/message',middlewares.authenticate , message.getAllMessages);
	app.get(API_PREFIX +'/message/:id', message.getMessage);
};

