'use strict';

require('dotenv').config();

module.exports = {
    test: getConf('test'),
    staging: getConf('staging'),
    production: getConf('production'),
    development: getConf('development')
};

/* eslint-disable */

function getConf(env) {

    return {
        client: 'sqlite3',
        connection: {
            filename: './database/data/data.db'
        },
        migrations: {
            directory: './database/migrations',
            tableName: 'knex_migrations'
        },
        seeds: {
            directory: `./database/seeds/${env}`
        },
        useNullAsDefault: true
    };
};
