import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { SignUpService } from './signup.service';

import { debounceTime, switchMap, map, first, tap } from 'rxjs/operators';

@Injectable()
export class UserNotTakenValidatorService {

    constructor(private signUpService: SignUpService) { }

    checkUserEmailTaken() {

        return (control: AbstractControl) => {
            return control
                .valueChanges
                .pipe(debounceTime(300))
                .pipe(switchMap(email =>
                    this.signUpService.checkUserEmailTaken(email)
                ))
                .pipe(map(isTaken => isTaken ? { useEmailTaken: true } : null))
                .pipe(tap(r => console.log(r)))
                .pipe(first());
        };
    }
}
