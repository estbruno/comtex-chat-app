const uuidv4 = require('uuid/v4');
const Password = require('../../lib/password-utils');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

module.exports = (bookshelf) => {

	var Model = require('./../models')(bookshelf);

	/* Save a user */
	var saveUser = function (req, res) {

		if (!req.body.name || !req.body.email || !req.body.password) {
			return res.status(400).json('Bad Request');
		}

		var newData = {
			id: uuidv4(),
			name: req.body.name,
			email: req.body.email,
			password: Password.cryptSync(req.body.password)
		}

		new Model.User().save(newData)
			.then(function (user) {
				res.json(user);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});

	};

	/* Get all users */
	var getAllUsers = function (req, res) {
		new Model.User().query(function (qb) {
				qb.select([
					'id', 'name', 'email'
				]);
			}).fetchAll()
			.then(function (users) {
				res.json(users);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
	};

	/* Get all users with invite status */
	var getAllUsersWithInviteStatusByRoomId = function (req, res) {

		var room_id = req.params.room_id;

		new Model.Room().where('id', room_id).fetch().then(function (data) {
			if (!data) {
				return res.status(404).json('Rooom Not found');
			}
			var room = data;

			new Model.User().query(function (qb) {
				qb.distinct([
					'name', 'room_allowed_user.id as room_allowed_user_id','room_id','user.id as user_id'
				]);
				qb.distinct('user.id');
				qb.leftJoin('room_allowed_user', 'user.id', 'room_allowed_user.user_id');

				//room owner does not return to listing
				qb.whereNot('user.id', room.attributes.user_id) 
			}).fetchAll()
			.then(function (users) {
				res.json(users);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
		}).catch(function (error) {
			console.log(error);
			res.status(500).json('An error occured');
		});

		
	};

	/* Delete a user */
	var deleteUser = function (req, res) {
		var userId = req.params.id;

		new Model.User().where('id', userId)
			.destroy()
			.then(function () {
				res.json('user deleted');
			})
			.catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
	};

	/* Get a user */
	var getUser = function (req, res) {
		var userId = req.params.id;

		new Model.User().where('id', userId)
			.fetch()
			.then(function (data) {

				if (!data) {
					return res.status(404).json('Not found');
				}

				var dataToReturn = _.pick(data.attributes, ['id', 'email', 'name']);
				return res.json(dataToReturn);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
	};

	/* Login */
	var loginUser = function (req, res) {
		var email = req.body.email;
		var password = req.body.password;

		if (!email || !password) {
			return res.status(400).json('Bad Request');
		}

		new Model.User().where('email', email)
			.fetch()
			.then(function (data) {

				if (!data) {
					return res.status(404).json('Not found');
				}

				/* password verification */
				if (!Password.compareSync(password, data.get('password'))) {
					return res.status(401).json('unauthorized');
				}

				/* data toreturn */
				var dataToReturn = _.pick(data.attributes, ['id', 'email', 'name']);

				/* setting token */
				const token = jwt.sign(dataToReturn, req.app.get('jwt_secret'), {
					expiresIn: 86400 // seconds, 24h
				});
				res.set('x-access-token', token);

				return res.json(dataToReturn);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
	};

	/* verify email */
	var isUserExist = function (req, res) {
		var email = req.params.email;

		new Model.User().where('email', email)
			.fetch()
			.then(function (data) {

				if (!data) {
					return res.json(false);
				}

				return res.json(true);
			}).catch(function (error) {
				console.log(error);
				res.status(500).json('An error occured');
			});
	};

	return {
		saveUser,
		getAllUsers,
		deleteUser,
		getUser,
		loginUser,
		isUserExist,
		getAllUsersWithInviteStatusByRoomId
	}
}