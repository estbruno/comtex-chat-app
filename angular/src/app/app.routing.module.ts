import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './errors/not-found/not-found.component';
import { ChatComponent } from './chat/chat.component';
import { RoomPublicListComponent } from './rooms/room-public-list/room-public-list.component';
import { AuthGuard } from './core/auth/auth.guard';
import { RoomUserListComponent } from './rooms/room-user-list/room-user-list.component';
import { RoomFormComponent } from './rooms/room-form/room-form.component';
import { RoomDeleteComponent } from './rooms/room-delete/room-delete.component';
import { RoomGuestListComponent } from './rooms/room-guest-list/room-guest-list.component';
import { RoomInviteUserComponent } from './rooms/room-invite-user/room-invite-user.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
    },
    {
        path: 'home',
        loadChildren: './home/home.module#HomeModule'
    },
    {
        path: 'chat/:room_id/:room_name',
        component: ChatComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'rooms',
        component: RoomPublicListComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'rooms/user',
        component: RoomUserListComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'rooms/guest',
        component: RoomGuestListComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'room/create',
        component: RoomFormComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'room/delete/:room_id',
        component: RoomDeleteComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'room/update/:room_id',
        component: RoomFormComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'room/invite/:room_id/:room_name',
        component: RoomInviteUserComponent,
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];

@NgModule({
    imports: [
        // RouterModule.forRoot(routes)
        RouterModule.forRoot(routes, { useHash: true })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }

