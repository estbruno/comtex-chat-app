const Colors = require('colors');
const Shell = require('shelljs');
const CommandExists = require('command-exists').sync;
const Exit = require('exit');

// Checks if programs / commands required by the script are in the system path.
const requiredCommands = ['knex'];

requiredCommands.forEach((command) => {

    if (!CommandExists(command)) {
        console.log(Colors.red(`\nCommand "${command}" required by this script is not available`));
        console.log(`Run "npm i -g ${command}" to install ${command} lib\n`);
        Exit();
    }
});

Shell.exec('knex seed:run --env development');