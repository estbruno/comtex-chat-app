import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NewUser } from './new-user';
import { environment } from './../../../environments/environment';

const API_URL = environment.api_url;

@Injectable()
export class SignUpService {

    constructor(private http: HttpClient) {}

    checkUserEmailTaken(email: string) {

        return this.http.get(API_URL + '/user/exists/' + email);
    }

    signup(newUser: NewUser) {
        return this.http.post(API_URL + '/user', newUser);
    }
}
