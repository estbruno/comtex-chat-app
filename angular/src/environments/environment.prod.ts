export const environment = {
  production: true,
  api_url: window.location.origin + '/service',
  socket_url: window.location.origin
};
