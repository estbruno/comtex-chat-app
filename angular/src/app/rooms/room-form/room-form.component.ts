import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { RoomService } from '../room/room.service';
import { UserService } from 'src/app/core/user/user.service';
import { PlatformDetectorService } from 'src/app/core/plataform-detector/platform-detector.service';
import { NewRoom } from '../room/new-room';
import { Room } from '../room/room';
import { RoomFull } from '../room/room-full';

@Component({
    templateUrl: './room-form.component.html'
})
export class RoomFormComponent implements OnInit {

    // View
    roomCreateForm: FormGroup;
    @ViewChild('nameInput') nameInput: ElementRef<HTMLInputElement>;
    marked = false;

    title = 'Create Room';
    userId: string;
    userName: string;
    roomId: string;
    constructor(private roomService: RoomService,
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService,
        private formBuilder: FormBuilder,
        private platformDetectorService: PlatformDetectorService) { }
    ngOnInit(): void {

        this.roomId = this.route.snapshot.paramMap.get('room_id');
        this.roomCreateForm = this.formBuilder.group({
            name: ['', Validators.required],
            isPublic: [false]
        });

        this.userId = this.userService.getUserId();
        this.userName = this.userService.getUserName();

        if (this.platformDetectorService.isPlatformBrowser()) {
            this.nameInput.nativeElement.focus();
        }

        console.log(this.roomId);

        if (this.roomId) {

            this.title = 'Update Room';

            this.roomService.getRoom(this.roomId).subscribe(

                room => {
                    this.roomCreateForm.setValue({ name: room.name, isPublic: room.is_public });
                },
                err => {
                    this.router.navigate(['']);
                }
            );
        }
    }

    saveRoom() {
        if (this.roomId) {
            this.updateRoom();
        } else {
            this.createRoom();
        }
    }

    createRoom() {
        const newRoom: NewRoom = {
            name: this.roomCreateForm.get('name').value,
            is_public: this.roomCreateForm.get('isPublic').value,
            user_id: this.userId
        };

        this.roomService.newRoom(newRoom).subscribe(
            () => this.router.navigate(['rooms/user']),
            err => {
                console.log(err);
                alert('Error, contact admin');
            }
        );
    }

    updateRoom() {
        const updateRoom: RoomFull = {
            id: this.roomId,
            name: this.roomCreateForm.get('name').value,
            is_public: this.roomCreateForm.get('isPublic').value
        };

        this.roomService.updateRoom(updateRoom).subscribe(
            () => this.router.navigate(['rooms/user']),
            err => {
                console.log(err);
                alert('Error, contact admin');
            }
        );
    }

    toggleVisibility(e) {
        this.marked = e.target.checked;
    }
}
