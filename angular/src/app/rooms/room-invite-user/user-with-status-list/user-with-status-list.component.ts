import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserWithRoomStatus } from '../../room/user-with-room-status';
import { RoomService } from '../../room/room.service';

@Component({
    selector: 'app-room-user-list-with-status',
    templateUrl: './user-with-status-list.component.html'
})
export class UserListWithStatusComponent implements OnInit {

    @Input() users: UserWithRoomStatus[];
    roomId: string;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private roomService: RoomService) { }
    ngOnInit(): void {

        this.roomId = this.route.snapshot.paramMap.get('room_id');
    }

    invite(user: UserWithRoomStatus) {

        this.roomService.addUsertoRoom(this.roomId, user.id).subscribe(
            status => {

                user.room_allowed_user_id = status;
                user.room_id = this.roomId;
            }, err => {
                alert(err);
            }
        );
    }

    removeInvite(user) {

        this.roomService.removeUsertoRoom(this.roomId, user.id).subscribe(status => {

            if (status === 'user was not invited' || status === 'invitation removed') {
                user.room_allowed_user_id = null;
                user.room_id = null;
            }
        });
    }

}
